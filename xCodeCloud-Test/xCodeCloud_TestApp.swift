//
//  xCodeCloud_TestApp.swift
//  xCodeCloud-Test
//
//  Created by Ivan Voloshchuk on 13/04/23.
//

import SwiftUI

@main
struct xCodeCloud_TestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
